general threads_no Sd 4
general identifier_str Ss example_job_i
general model_code_str Ss LH_secretion_across_studies_model
general alg_code_str Ss MCMC_gibbs_sMMALA
general chain_repeats Sd 4
general rows_per_dataset_entry Sd 4
general experiment_CV Vd 0.042
general experiment_CV_2 Vd 0.042
SMC nparticles Sd 1000
cSMC_AS nparticles Sd 10
cSMC_AS AS_probability Sd 1
MCMC_gibbs_sMMALA hmc_theta_dim Sd 5
MCMC_gibbs_sMMALA mmala_blocks_no Sd 2
MCMC_gibbs_sMMALA block_start_indices Vd     0,3,5,6,7,8,9
MCMC_gibbs_sMMALA iterations Sd 300000
MCMC_gibbs_sMMALA epsilon Sd 0.01
MCMC_gibbs_sMMALA steps Sd 20
MCMC_gibbs_sMMALA iter_adapt Sd 10000
MCMC_gibbs_sMMALA delta Sd 0.234
MCMC_gibbs_sMMALA report_every Sd 100
MMCMC_gibbs_sMMALA i_param_names Vs k,d,f,k_i,f_i,mu_tau_on,mu_tau_on_i,mu_tau_off,mu_tau_off_i
MCMC_gibbs_sMMALA initial_values Vd     2,-2,2,2,2,1.1376,1.1376,1.7571,1.7571
MCMC_gibbs_sMMALA min_values     Vd    -100,-100,-100,-100,-100, 0,0,1,1
MCMC_gibbs_sMMALA max_values     Vd     100,100,100,100,100,2.3802,2.3802,2.3802,2.3802
LH_secretion_across_studies_model dt Sd 1

            
            
            
            
            
            
            

