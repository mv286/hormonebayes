HormoneBayes
=======================

HormoneBayes is an open-access software tool for analysing serial hormone measurements to assess pulsatility.

# Installation

1. **Download and install Docker**.  Download Docker [here](https://www.docker.com/get-started) and run the installer. You can learn more about Docker [here](https://docs.docker.com/desktop/).

2. **Download the latest version of hormoneBayes**. Download the latest stable version of hormoneBayes [here](https://git.exeter.ac.uk/mv286/hormonebayes/-/tree/master/latest_version?ref_type=heads) and extract the files in a local folder on your computer. A pdf with step-by-step instruction of how run hormoneBayes can be found in the installation folder.

# Running HormoneBayes

1. Start the Docker daemon
2. Run the executable *hormoneBayes* in the folder where you saved the hormoneBayes files

# Analysing data 

1.	**Load data**. Press the "Load data …" button and select the data file. To test the application navigate to the folder where you extracted the application and select data_example.csv. A message will appear saying that the file was loaded successfully.

2.	**Load settings**. Press the "Load settings …" button and select the settings file. To test the application navigate to the folder where you saved the application and select settings_template.csv. This file can be used as a template for analysisng other datasets as well. A message will appear saying that the settings file has been loaded successfully. Press the "View settings button, change the *Job identifier* field and press the "Close" button.

3.	**Start the analysis**. Once data and settings have been loaded you can hit the "Start" button to start the analysis. A message will appear saying that analysis has started and you can track progress in the console window. Once the analysis has finished output files (named using the *Job identifier* you specified in the settings; see step 2) will appear in the *out* folder within the application folder.

4. **View results**.  Results are stored as text files. When analysis has finished you can inspect the results by pressing the "View results" button. In the dialog box input the *Job identifier* you used in the settings (see step 2). HormoneBayes will generate a grpahical summary of the results. A text summary of the results will also be generated stored in the ‘out’ folder within the application folder.

#  Input data file

The data file (.csv) should present each dataset as two lines of comma-separated values. The first line should correspond to the measurement times, and the second line to the hormone readouts.

Multiple datasets can be included in a single data file.  HormoneBayes will process these datasets sequentially, generating separate output files for each dataset. The output files will be named with the *job identifier* (specified in the settings) followed by a number indicating the position of the dataset within the data file.

# HormoneBayes output 

When the analysis of a dataset has finished output files will appear in the "out" folder. 

The naming format of the output files is:

\[job indetifier\]\_\[dataset index\]\_\[chain index\]\_\[result file\].csv

where the \[result file\] specifies the type of output:

1. **parameters**: contains the parameters values sampled from the posterior at each iteration of the MCMC algorithm.
2. **trajectories**: contains the model dynamics sampled  by the MCMC algorithm.
3. **lik**: the corresponding likelihood  at each iteration of the MCMC algorithm.

A summary file ("_parameter_summary.csv") will also be created by HormoneBayes when the user presses the "View Results" button. This file contains estimates of the model parameters along with an estimate of the pulse frequency and amplitude. 

Also, while running hormoneBayes reports its progress to the console widow. Below is an example of this output:

> thread 0 --- iteration 3700/10000(time 2.79229): log_lik -411.063 | theta ( 2.44461 -2.17691  4.72891  1.36202  1.41496) 

> thread 0 --- iteration 3800/10000(time 2.63041): log_lik -312.584 | theta ( 2.44081 -2.12229     2.42  1.81495   1.9231) 

where you can see the iteration number, log-likehood value and current parameter vector (theta).By default, this information is written every 100 iterations, but the user can change the output frequency by modyfing the  *report_every* entry in the settings file.

#  Settings 

The user can use the template setting file to load settings Furhtemore, the user interface allows the user to set various parameters via the "View settings" button: 

1. **Job configuration parameters**

    1. Job indetifier: An alphanumeric that will be used to name the output files
    2. Number of independent chains: How many independent chains will be run by the MCMC sampler. Good practise is to use several (often 4 or more) independent chains to access convergence of the sampler.
    3. Number of threads. The number of parallel threads that will be used to run the MCMC sampler.
    4. Parameteric model. Specifies the parametric model to be used in the analysis. The 'LH_secretion_model' (used in the template setting file) should be used for the analysis of LH datasets. Future releases of HormoneBayes will provide additional models. 
    5. Inference method. Specifies which MCMC sampler to be used in the analysis. This parameter is reserved for future releases of HormoneBayes, and the value 'MCMC_gibbs_hmc' (used in the template setting file) is the only available choice. 

2. **Model parameters**. Parameters used by the parametric model. Here the user can specify the internal timestep of the model (dt; in minutes) as well as the measuremt error (as a coeficient of variation, CV, in demical form). 

3. **MCMC parameters**. Here the user can specifiy the length of each MCMC chain, as well well as the number of iterations that will be used in the initial adaptive part of the algorithm, and which will be discarded from the analysis. 

The user can also change setting values by chaning the corresponing line in the settings file. For example to set the number of threads to 2 the user can modify the following line in the settings file

> general threads_no Sd 2

Other settings: 

1. general identifier_str Ss *job_name* : set the job indetifier to *job_name* (alphanumeric)
2. general chain_repeats Sd *chain_number*: set the number of independent chains to *chain_number* (integer value)
3. general chain_repeats Sd *seed*: set the seed for the pseudo-random number generator to *seed* (integer value)
4. general experiment_CV Vd *CV*: set the measuremt error to *CV* (given in demical form, e.g., if CV is 5% then the value 0.05 should be used)

# Changing model parameters' priors

The user can change the priors for model parameters by modyfing the setting file. 

The prior for the secretion rate parameter is a log-normal, i.e., $log(k) ~ N(\mu,\sigma)$. The user can change the parameters of the log-normal by including the following lines in the setting file:
1. LH_secretion_model prior_mean_B_1 Sd *mu_value*: 
set the mean of the log-normal prior to *mu_value* (Default value 0)
2. LH_secretion_model prior_std_B_1 Sd *std_value*: set the st. dev. of the log-normal prior  to *std_value* (Default value 5)

The prior for the pulsatility parameter is a Beta distribution, $Beta(\alpha,\beta)$. The user can change the parameters of this prior by including the following lines in the settings file:
1. LH_secretion_model f_Beta_prior_alpha Sd *alpha_value*: set the $\alpha$ parameter of the Beta distribution to *alpha_value* (Default value 1)
2. LH_secretion_model f_Beta_prior_beta Sd *beta_value*: set the $\beta$ parameter of the Beta distribution to *beta_value* (Default value 1)


The prior for the half life of LH is a Normal distribution, $N(\mu,\sigma)$. The user can change the parameters of this prior by including the following lines in the settings file:
1. LH_secretion_model prior_mean_alpha_1 Sd *mu_value*: set the mean of the normal prior to *mu_vaue* (Default value 80)
2. LH_secretion_model prior_std_alpha_1 Sd *std_value*: set the st. dev. of the normal prior  to *std_value* (Default value 9.3)

# Assessing the effect of pharmacological interventions on LH pulsatility using HormoneBayes

HormoneBayes can be used to analyse the effect of pharmacological interventions on LH pulsatility. 

To perform such an analysis the user should use the *setting_template_comp.txt* file when loading the settings. Under the hood the file instructs HormoneBayes to use a modified model (*LH_secretion_across_studies_model*) to perform the inference. 

Also, the data file should present each datasets as four lines of comma-separated values: the first line should correspond to the measurement times in the control case; the second line to the hormone readouts in the control case; the thrid case to the the measurement times in the intervention case; the third line to the hormone readouts in the intervention case.

# Implementation details

HormoneBayes has been implemented in C++, using the BOOST C++ libraries (v1.83.0; https://www.boost.org) and the Eigen C++ template library for linear algebra (v3.4.0,https://eigen.tuxfamily.org/). HormoneBayes uses openMP API for multithreading. 

# Data

HormoneBayes has been validated on the following human datasets

1. Narayanaswamy S, Prague JK, Jayasena CN, et al. Investigating the KNDy Hypothesis in Humans by Coadministration of Kisspeptin, Neurokinin B, and Naltrexone in Men. J Clin Endocrinol Metab 2016; 101(9): 3429-36. [doi:10.1210/jc.2016-1911](https://10.1210/jc.2016-1911)

2. Narayanaswamy S, Jayasena CN, Ng N, et al. Subcutaneous infusion of kisspeptin-54 stimulates gonadotrophin release in women and the response correlates with basal oestradiol levels. Clin Endocrinol (Oxf) 2016; 84(6): 939-45. [doi:10.1111/cen.12977](https://10.1111/cen.12977)

3. Prague JK, Roberts RE, Comninos AN, et al. Neurokinin 3 receptor antagonism as a novel treatment for menopausal hot flushes: a phase 2, randomised, double-blind, placebo-controlled trial. Lancet 2017; 389(10081): 1809-20. [doi:10.1016/S0140-6736(17)30823-1](https://doi.org/10.1016/S0140-6736(17)30823-1)

4. Jayasena CN, Abbara A, Veldhuis JD, et al. Increasing LH pulsatility in women with hypothalamic amenorrhoea using intravenous infusion of Kisspeptin-54. J Clin Endocrinol Metab 2014; 99(6): E953-61. [doi:10.1210/jc.2013-1569](https://10.1210/jc.2013-1569)


## Support ##

Contact m.voliotis@exeter.ac.uk

