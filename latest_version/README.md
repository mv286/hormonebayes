# Installation

1. Download hormoneBayes_macos_win.zip to run  hormoneBayes on a Windows machine
2. Download hormoneBayes_macos_Intel.zip to run  hormoneBayes on MacOS (Intel chip). 
3. Download hormoneBayes_macos_Mchip.zip to run  hormoneBayes on MacOS (M chip)

Note: On MacOS you might need to give the executable *hormonebayes* execution rights by opening a terminal and running the following command: 

> \>chmod +x hormonebayes

